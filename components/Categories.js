import React, {useEffect, useState} from "react";
import {ScrollView, Text, View} from "react-native";
import CategoryCard from "./CategoryCard";
import client, {urlFor} from "../sanity";
import category from "../sanity/schemas/category";

const Categories = () => {
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        client.fetch(`
        *[_type == "category"] 
        `
        ).then((data => {
            setCategories(data)
        }))
    }, [])

    return(
        <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{
                paddingHorizontal: 15,
                paddingTop: 10
            }}
        >
            {/* CategoryCard.js */}
            {categories.map( (category) => (
                <CategoryCard
                    imgUrl={urlFor(category.image).width(200).url()}
                    title={category.name}
                    key={category._id}
                />
                ))}
        </ScrollView>
    )
}

export default Categories