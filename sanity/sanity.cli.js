import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'p1d4glie',
    dataset: 'delivery'
  }
})
