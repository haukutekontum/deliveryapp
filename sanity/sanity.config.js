import {defineConfig} from 'sanity'
import {deskTool} from 'sanity/desk'
import {visionTool} from '@sanity/vision'
import {schemaTypes} from './schemas'

export default defineConfig({
  name: 'default',
  title: 'deliveryApp',

  projectId: 'p1d4glie',
  dataset: 'delivery',

  plugins: [deskTool(), visionTool()],

  schema: {
    types: schemaTypes,
  },
})
